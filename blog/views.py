from django.contrib.auth.models import  Group
from authentication.models import Account
from django.shortcuts import render
from .serializer import UserSerializer,GroupSerializer,PostSerializer
from authentication.serializers import AccountSerializer
from .models import Post

from rest_framework import viewsets, status
from rest_framework.decorators import detail_route, list_route, permission_classes, authentication_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000

@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


@permission_classes((IsAuthenticated, ))
@authentication_classes((JSONWebTokenAuthentication,))
class UserViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    pagination_class = LargeResultsSetPagination

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
