
from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from blog import views

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

router = routers.DefaultRouter()
router.register(r'users',views.UserViewSet)
router.register(r'groups',views.GroupViewSet)
router.register(r'post',views.GroupViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #Resurce Router
    url(r'^',include(router.urls)),

    # JWT token
    url(r'^v1/authentication/',include('authentication.urls')),
    #url(r'^api-token-auth/', obtain_jwt_token),
    #url(r'^api-token-refresh/', refresh_jwt_token),
    #url(r'^api-token-verify/', verify_jwt_token),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
