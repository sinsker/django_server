from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from .views import  AuthRegister

urlpatterns = [
    url(r'^login', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),

    url(r'^register$', AuthRegister.as_view()),

    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
