from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# Create your models here.

class AccountManager(BaseUserManager):

    def create_user(self, email, password=None, **kwargs):
        # Ensure that an email address is set

        if not email:
            raise ValueError('Users must have a valid e-mail address!!!!!!!!')

        # Ensure that a username is set
        if not kwargs.get('name'):
            raise ValueError('Users must have a valid name!!!!!!!!!!!')

        account = self.model(
            email=self.normalize_email(email),
            name=kwargs.get('name'),
            sex=kwargs.get('sex', None),
            job=kwargs.get('job', None),
        )

        account.set_password(password)
        account.save()

        return account

    def create_superuser(self, email, password=None, **kwargs):
        account = self.create_user(email, password, **kwargs)

        account.is_admin = True
        account.save()

        return account

class Account(AbstractBaseUser):

    email = models.EmailField(unique=True)
    name = models.CharField(max_length=50)

    sex = models.CharField(max_length=1, null=True)
    job = models.CharField(max_length=100, null=True)

    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']


    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

