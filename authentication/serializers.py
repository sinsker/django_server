from django.contrib.auth import update_session_auth_hash
from rest_framework import serializers
from .models import Account

class AccountSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    confirm_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = Account
        fields = ('email','name','password', 'confirm_password', 'created_date', 'modified_date')
        read_only_fields = ('created_date','modified_date')

    def create(selfself, validated_data):
        return Account.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email',instance.email)
        instance.name = validated_data.get('name', instance.name)
        instance.sex = validated_data.get('sex', instance.sex)
        instance.job = validated_data.get('job', instance.job)
        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and password == confirm_password:
            instance.set_password(password)

        instance.save()
        return instance

    def validate(self, data):
        '''
        패스워드 비교 체크
        '''
        if data['password']:
            if data['password'] != data['confirm_password']:
                raise serializers.ValidationError("패스워드와 패스워드 확인이 동일하지 않습니다.")
        return data




